package echec.threads;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

import echec.ihm.Fenetre;
import echec.jeu.Coup;

public class Connexion implements Runnable
{
	private String adresseIP;
	private Socket socket;
	private ArrayBlockingQueue<Coup> file;
	private Fenetre fenetre;

	public Connexion(String adresseIP, ArrayBlockingQueue<Coup> file, Fenetre fenetre)
	{
		this.adresseIP = adresseIP;
		this.file = file;
		this.fenetre = fenetre;
	}

	@Override
	public void run()
	{
		boolean choixCouleur = false;
		try
		{
			this.socket = new Socket(this.adresseIP, 25565);
			choixCouleur = true;
		} catch (Exception e)
		{
			// Si la connexion n'a pas pu etre etablie
			ServerSocket serverSocket = null;
			try
			{
				serverSocket = new ServerSocket(25565);
			} catch (IOException e2)
			{
				e2.printStackTrace();
			}
			boolean connecte = false;
			while (!connecte)
			{
				try
				{
					this.socket = serverSocket.accept();
					connecte = true;
				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		Thread jouerThread = new Thread(new Jouer(this.socket, this.file, choixCouleur, this.fenetre));
		jouerThread.start();
	}

}
