package echec.threads;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JOptionPane;

import echec.ihm.Fenetre;
import echec.jeu.Coup;
import echec.utilitaires.Base64Coder;

public class Jouer implements Runnable
{

	public Socket socket;
	private ArrayBlockingQueue<Coup> file;
	private boolean couleur;
	private boolean finDePartie;
	private BufferedReader in;
	private PrintWriter out;
	private Fenetre fenetre;

	public Jouer(Socket socket, ArrayBlockingQueue<Coup> file, boolean couleur, Fenetre fenetre)
	{
		this.socket = socket;
		this.file = file;
		this.couleur = couleur;
		this.finDePartie = true;
		this.fenetre = fenetre;

		try
		{
			this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.out = new PrintWriter(socket.getOutputStream());
		} catch (Exception e)
		{
			e.getStackTrace();
		}
	}

	@Override
	public void run()
	{
		JOptionPane.showMessageDialog(null, "Vous etes " + (this.couleur ? "noir" : "blanc, vous commencez ") + " ! ", "Choix couleur", JOptionPane.INFORMATION_MESSAGE);
		while (finDePartie)
		{
			// En attente du deplacement adverse
			if (this.couleur != this.fenetre.echiquier.tour)
			{
				this.fenetre.statusBar.setMessage("A votre adversaire de jouer");
				try
				{
					String buffer = in.readLine();
					Coup coup = (Coup) fromString(buffer);
					this.fenetre.echiquier.executer(coup);
					this.fenetre.chargerEchiquier(this.fenetre.echiquier);

				} catch (Exception e)
				{
					e.getStackTrace();
				}
			}
			// A vous de jouer
			else
			{
				this.fenetre.statusBar.setMessage("A vous de jouer");
				try
				{
					Coup coup = file.take();
					out.println(toString(coup));
					out.flush();

				} catch (Exception e)
				{
					e.getStackTrace();
				}
			}
			this.fenetre.echiquier.tour = !this.fenetre.echiquier.tour;
		}
	}

	private static Object fromString(String s) throws IOException, ClassNotFoundException
	{
		byte[] data = Base64Coder.decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	private static String toString(Serializable o) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		return new String(Base64Coder.encode(baos.toByteArray()));
	}

}
