package echec.jeu;

import java.util.Stack;

public class HistoriqueCoup
{
	private Stack<Coup> pile;
	
	@Override
	public String toString() {
		return "HistoriqueCoup [pile=" + pile + "]";
	}

	public HistoriqueCoup()
	{
		this.pile = new Stack<Coup>();
	}
	
	public Stack<Coup> getPile()
	{
		return this.pile;
	}
	
	public void setPile(Stack<Coup> pile)
	{
		this.pile = pile;
	}
}
