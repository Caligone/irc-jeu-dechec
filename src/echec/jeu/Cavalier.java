package echec.jeu;

@SuppressWarnings("serial")
public class Cavalier extends Piece{
	
	public Cavalier(boolean jeu, int x, int y)
	{
		super(jeu, x, y);
	}
	
	public boolean deplacer(int x, int y){
		if(((x>=0 || x<8) && (x>=0 || x<8)) && (
		(y==this.y-2 && x==this.x-1) ||
		(y==this.y-2 && x==this.x+1) ||
		(y==this.y-1 && x==this.x-2) ||
		(y==this.y-1 && x==this.x+2) ||
		(y==this.y+1 && x==this.x-2) ||
		(y==this.y+1 && x==this.x+2) ||
		(y==this.y+2 && x==this.x-1) ||
		(y==this.y+2 && x==this.x+1))){
			//System.out.println("deplacer cavalier return true");
			return true;
		}
		//System.out.println("deplacer cavalier return false");
		return false;
	}
}