package echec.jeu;

@SuppressWarnings("serial")
public class Pion extends Piece{
	
	public Pion(boolean jeu, int x, int y) {
		super(jeu, x, y);
	}

	public boolean deplacer(int x, int y){
		if(((x>=0 || x<8) && (y>=0 || y<8)) && (
		(this.y==6 && y==(this.y-2) && x==this.x) || // cas du premier déplacement à deux cases
		(y==this.y-1 && x==this.x) ||
		(y==this.y-1 && x==this.x+1) || // et pièce adverse
		(y==this.y-1 && x==this.x-1) || // et pièce adverse
		(this.y==1 && y==(this.y+2) && x==this.x) || // cas du premier déplacement à deux cases
		(y==this.y+1 && x==this.x) ||
		(y==this.y+1 && x==this.x+1) || // et pièce adverse
		(y==this.y+1 && x==this.x-1) )){ // et pièce adverse
			//System.out.println("deplacer pion return true");
			return true;
		}
		//System.out.println("deplacer pion return false");
		return false;
	}
}