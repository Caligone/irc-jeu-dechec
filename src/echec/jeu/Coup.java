package echec.jeu;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Coup implements Serializable
{
	Piece piece;
	int xInit;
	int yInit;
	int xFin;
	int yFin;
	
	public Coup(Piece piece, int xInit, int yInit, int xFin, int yFin)
	{
		this.piece = piece;
		this.xInit = xInit;
		this.yInit = yInit;
		this.xFin = xFin;
		this.yFin = yFin;
	}
	
	@Override
	public String toString() {
		return "Coup [piece=" + piece.getClass().getSimpleName() + ", xInit=" + xInit + ", yInit=" + yInit
				+ ", xFin=" + xFin + ", yFin=" + yFin + "]";
	}


	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	public int getxInit() {
		return xInit;
	}
	public void setxInit(int xInit) {
		this.xInit = xInit;
	}
	public int getyInit() {
		return yInit;
	}
	public void setyInit(int yInit) {
		this.yInit = yInit;
	}
	public int getxFin() {
		return xFin;
	}
	public void setxFin(int xFin) {
		this.xFin = xFin;
	}
	public int getyFin() {
		return yFin;
	}
	public void setyFin(int yFin) {
		this.yFin = yFin;
	}
	
}
