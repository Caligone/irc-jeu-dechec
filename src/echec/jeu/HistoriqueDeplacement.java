package echec.jeu;

import java.util.Stack;

/**
 * Represente un historique de deplacement TODO Implementer cet historique dans
 * la JList
 */
public class HistoriqueDeplacement
{
	private Stack<Deplacement> pile;

	/**
	 * Constructeur HistoriqueDeplacement
	 */
	public HistoriqueDeplacement()
	{
		this.pile = new Stack<Deplacement>();
	}

	/**
	 * Retourne la pile de deplacements
	 * 
	 * @return pile
	 */
	public Stack<Deplacement> getPile()
	{
		return this.pile;
	}

	/**
	 * Defini la pile de deplacement
	 * 
	 * @param pile
	 */
	public void setPile(Stack<Deplacement> pile)
	{
		this.pile = pile;
	}
}
