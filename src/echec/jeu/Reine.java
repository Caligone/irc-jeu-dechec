package echec.jeu;

@SuppressWarnings("serial")
public class Reine extends Piece{
	
	public Reine(boolean jeu, int x, int y)
	{
		super(jeu,x ,y);
	}
	
	
	public boolean deplacer(int x, int y){
		int tempx=x-this.x, tempy=y-this.y;
		if( ((x>=0 || x<8) && (x>=0 || x<8))&&
		(Math.abs(tempx)-Math.abs(tempy)==0) ||
		((y==this.y) ||
		(x==this.x) )){
			//System.out.println("deplacer reine return true");
			return true;
		}
		//System.out.println("deplacer reine return false");
		return false;
	}
}