package echec.jeu;

import java.io.Serializable;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * Represente un Echiquier d'un point de vue metier
 */
@SuppressWarnings("serial")
public class Echiquier implements Serializable {

    private Boolean termine;
    private Jeu[] jeu = new Jeu[2];
    private DefaultListModel listeCoups;
    private HistoriqueCoup historique;
	private ArrayBlockingQueue<Coup> file;
	public boolean tour;
	
    public Echiquier()
    {
        this.jeu[0] = new Jeu(false);
        this.jeu[1] = new Jeu(true);
        this.listeCoups = new DefaultListModel();
        this.termine = false;
        this.tour = false;
		this.setFile(new ArrayBlockingQueue<Coup>(10));
        this.setHistorique(new HistoriqueCoup());
    }
    
    public Integer verifierEtat() {
        return null;
    }

    public Boolean isTermine() {
        return this.termine;
    }
    
    public Jeu[] getJeu()
    {
        return this.jeu;
    }
    
    public void setJeu(Jeu[] jeu)
    {
        this.jeu = jeu;
    }
    
    public DefaultListModel getListeCoup()
    {
        return this.listeCoups;
    }
    
    public void getListeCoup(DefaultListModel listeCoups)
    {
        this.listeCoups = listeCoups;
    }
    
    public boolean deplacer(Piece p, int x, int y, boolean calcEchec){
        int i, j, pasx, pasy;
        boolean erreur=false, tmp, echec=false;
        Piece tourRoque;
        
        // Calcul du pas de deplacement honrizontal
        if(x-p.x!=0)
            pasx=(x-p.x)/Math.abs(x-p.x);
        else
            pasx=0;
        // Calcul du pas de deplacement vertical
        if(y-p.y!=0)
            pasy=(y-p.y)/Math.abs(y-p.y);
        else
            pasy=0;
        // Si le deplacement est valide pour la piece
        if(p.deplacer(x, y)){
            // Si l'objet n'est pas un cavalier (car celui-ci peut sauter par dessus les autres pieces)
            if(!(p instanceof Cavalier)){
                // Traitement du cas specifique du pion qui ne peut pas reculer
                if(p instanceof Pion){
                    if(p.getCouleur() && y<p.y)
                        erreur=true;
                    if(!(p.getCouleur()) && y>p.y)
                        erreur=true;
                }
                if(erreur==false){
                    if(pasx==0){ // Cas d'un deplacement sur une colonne
                        i=p.x+pasx;
                        for(j=p.y+pasy;j!=y+pasy;j=j+pasy){
                            tmp=checkMove(p, x, y, i, j, calcEchec);
                            if(tmp)
                                erreur=tmp;
                        }
                    }else{
                        if(pasy==0){ // Cas d'un deplacement sur une ligne
                            j=p.y+pasy;
                            for(i=p.x+pasx;i!=x+pasx;i=i+pasx){
                                tmp=checkMove(p, x, y, i, j, calcEchec);
                                if(tmp)
                                    erreur=tmp;
                            }
                        }else{ // Cas d'un deplacement en diagonale
                            for(i=p.x+pasx, j=p.y+pasy ; i!=x+pasx && j!=y+pasy ; i=i+pasx, j=j+pasy){
                                tmp=checkMove(p, x, y, i, j, calcEchec);
                                if(tmp)
                                    erreur=tmp;
                            }
                        }
                    }
                }
            }else{
                tmp=checkMove(p, x, y, x, y, calcEchec);
                if(tmp)
                    erreur=tmp;
            }
        }else
            erreur=true;
        if(p instanceof Roi && (p.x-3==x || p.x+2==x) && ((Roi) p).moved==true){
            erreur=true;
        }
        if(erreur==false){
            // Traitement specifique roi (roque)
            if(p instanceof Roi && ((Roi) p).moved==false && (x==1 || x==6)){
                if(p.getCouleur() && p.x==4 && p.y==0){ // Cas Roi noir
                    if(x==6){ // Cas roque droit
                        // Recuperation pi�ce droite
                        tourRoque=estOccupee(7, 0);
                        // Test presence tour droite
                        if(tourRoque instanceof Tour && ((Tour) tourRoque).moved==false){
                             if(calcEchec==false){
                                 if(!(SeMetEnEchec(p, x, y))){
                                     // Placement du roi
                                     p.x=x;
                                     p.y=y;
                                     ((Roi) p).moved=true;
                                     // Placement de la tour
                                     tourRoque.x=x-1;
                                     tourRoque.y=y;
                                     JOptionPane.showMessageDialog(null,
                                             "Joueur "+(p.getCouleur()? "Noir" : "Blanc")+" a roque !",
                                             "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                     if(MetEnEchec(p.couleur, x, y))
                                         JOptionPane.showMessageDialog(null,
                                                 "Joueur "+(p.getCouleur()? "Blanc" : "Noir")+", vous etes en echec !",
                                                 "Deplacement impossible", JOptionPane.INFORMATION_MESSAGE);
                                 }else
                                        echec=true;
                            }
                        }
                    }else{ // Cas roque gauche
                        if(x==1){
                            // Recuperation pi�ce gauche
                            tourRoque=estOccupee(0, 0);
                            // Test presence tour gauche
                            if(tourRoque instanceof Tour && ((Tour) tourRoque).moved==false){
                                 if(calcEchec==false){
                                     if(!(SeMetEnEchec(p, x, y))){
                                        // Placement du roi
                                        p.x=x;
                                        p.y=y;
                                        ((Roi) p).moved=true;
                                        /// Placement de la tour
                                        tourRoque.x=x+1;
                                        tourRoque.y=y;
                                        JOptionPane.showMessageDialog(null,
                                            "Joueur "+(p.getCouleur()? "Noir" : "Blanc")+" a roque !",
                                            "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                        if(MetEnEchec(p.couleur, x, y))
                                            JOptionPane.showMessageDialog(null,
                                                    "Joueur "+(p.getCouleur()? "Blanc" : "Noir")+", vous etes en echec !",
                                                    "Deplacement impossible", JOptionPane.INFORMATION_MESSAGE);
                                    }else
                                        echec=true;
                                }
                            }
                        }
                    }
                }else{ // Cas Roi blanc
                    if(p.getCouleur()==false && p.x==4 && p.y==7){ // Cas Roi blanc
                        if(x==6){ // Cas roque droit
                            // Recuperation pi�ce droite
                            tourRoque=estOccupee(7, 7);
                            if(tourRoque!=null)
                            // Test presence tour droite
                            if(tourRoque instanceof Tour){
                                 if(calcEchec==false){
                                     if(!(SeMetEnEchec(p, x, y))){
                                         // Placement du roi
                                         p.x=x;
                                         p.y=y;
                                         ((Roi) p).moved=true;
                                         // Placement de la tour
                                         tourRoque.x=x-1;
                                         tourRoque.y=y;
                                         JOptionPane.showMessageDialog(null,
                                            "Joueur "+(p.getCouleur()? "Noir" : "Blanc")+" a roque !",
                                            "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                         if(MetEnEchec(p.couleur, x, y))
                                             JOptionPane.showMessageDialog(null,
                                                     "Joueur "+(p.getCouleur()? "Blanc" : "Noir")+", vous etes en echec !",
                                                     "Deplacement impossible", JOptionPane.INFORMATION_MESSAGE);
                                     }else
                                            echec=true;
                                 }
                            }
                        }else{ // Cas roque gauche
                            if(x==1){
                                // Recuperation pi�ce gauche
                                tourRoque=estOccupee(0, 7);
                                // Test presence tour gauche
                                if(tourRoque instanceof Tour){
                                     if(calcEchec==false){
                                         if(!(SeMetEnEchec(p, x, y))){
                                             // Placement du roi
                                             p.x=x;
                                             p.y=y;
                                             ((Roi) p).moved=true;
                                             /// Placement de la tour
                                             tourRoque.x=x+1;
                                             tourRoque.y=y;
                                             JOptionPane.showMessageDialog(null,
                                                "Joueur "+(p.getCouleur()? "Noir" : "Blanc")+" a roque !",
                                                "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                             if(MetEnEchec(p.couleur, x, y))
                                                 JOptionPane.showMessageDialog(null,
                                                         "Joueur "+(p.getCouleur()? "Blanc" : "Noir")+", vous etes en echec !",
                                                         "Deplacement impossible", JOptionPane.INFORMATION_MESSAGE);
                                         }else
                                                echec=true;
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                if(calcEchec==false){
                     if(!(SeMetEnEchec(p, x, y))){
                        p.x=x;
                        p.y=y;
                        if(p instanceof Roi){
                            ((Roi) p).moved=true;
                        }else{
                            if(p instanceof Tour){
                                ((Tour) p).moved=true;
                            }else{
                                if(p instanceof Pion){
                                    if((p.getCouleur() && y==7) || (!(p.getCouleur())&& y==0)){
                                        p=ChangerPiece(p);
                                    }
                                }
                            }
                        }
                        if(MetEnEchec(p.couleur, x, y))
                            JOptionPane.showMessageDialog(null,
                                    "Joueur "+(p.getCouleur()? "Blanc" : "Noir")+", vous etes en echec !",
                                    "Deplacement impossible", JOptionPane.INFORMATION_MESSAGE);
                    }else
                        echec=true;
                 }
            }
            if(echec==false){
                return true;
            }else{
                JOptionPane.showMessageDialog(null,
                        "Vous vous mettez en echec !",
                        "D�placement impossible", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }
        }else{
            return false;
        }
    }

    public boolean checkMove(Piece p, int x, int y, int i, int j, boolean calcEchec){
        boolean erreur=false;
        Piece temp=estOccupee(i,j);
        if((p instanceof Pion) && (estOccupee(x,y)==null) && x!=p.x){
            erreur=true;
        }
        if((p instanceof Pion) && (estOccupee(x,y)!=null) && x==p.x){
            erreur=true;
        }
        if((i!=x || j!=y) && temp!=null){
            // Si on est pas sur la case d'arrivee et qu'il y a une pi�ce sur le trajet
            // Alors le deplacement ne peut fonctionner
            erreur=true;
        }
        if((i==x || j==y) && temp!=null){
            // Si on est sur la case d'arrivee et qu'il y a une pi�ce sur cette case
            if(temp.getCouleur()==p.getCouleur()){
                // S'il s'agit d'une pi�ce de la couleur du joueur
                erreur=true;
            }else{
                // Si c'est une pi�ce de l'autre couleur on effectue le deplacement
                // et on "supprime" de la pi�ce adverse
                //System.out.println("Pi�ce supprimee : "+temp.getClass().getSimpleName()+" "+(temp.getCouleur() ? "Noir" : "Blanc")+" "+"("+temp.x+","+temp.y+")");
                //System.out.println("Remplacee par   : "+p.getClass().getSimpleName()+" "+(p.getCouleur() ? "Noir" : "Blanc")+" "+"("+p.x+","+p.y+")");
                if(calcEchec==false){
                    temp.x=-1;
                    temp.y=-1;
                }
                //System.out.println("Nouveaux emplacements :");
                //System.out.println("Pi�ce supprimee : "+temp.getClass().getSimpleName()+" "+(temp.getCouleur() ? "Noir" : "Blanc")+" "+"("+temp.x+","+temp.y+")");
                //System.out.println("Remplacee par   : "+p.getClass().getSimpleName()+" "+(p.getCouleur() ? "Noir" : "Blanc")+" "+"("+p.x+","+p.y+")");
            }
        }
        return erreur;
    }
    
    public Piece estOccupee(int x, int y) {
        for(Piece piece : this.jeu[0].pieces)
            if(piece.x==x && piece.y==y){
                return piece;
            }
        for(Piece piece : this.jeu[1].pieces)
            if(piece.x==x && piece.y==y){
                return piece;
            }
        return null;
    }
    
    public Piece getPieceFromXY(int x, int y)
    {
        for(Piece piece : this.getJeu()[0].pieces)
        {
            if(piece.getX() == x && piece.getY() == y)
                return piece;
        }
        for(Piece piece : this.getJeu()[1].pieces)
        {
            if(piece.getX() == x && piece.getY() == y)
                return piece;
        }
        return null;
    }

    public HistoriqueCoup getHistorique() {
        return historique;
    }

    public void setHistorique(HistoriqueCoup historique) {
        this.historique = historique;
    }
    
    public Piece ChangerPiece(Piece p){
        int i;
        Object[] possibilities = {"Reine", "Cavalier", "Fou", "Tour"};
        String s = (String)JOptionPane.showInputDialog(null,
                            "Choisissez la nouvelle forme de\nvotre Pion ("+p.x+", "+p.y+")",
                            "Customized Dialog",JOptionPane.PLAIN_MESSAGE,null,
                            possibilities,"Reine");
        for(i=0;i<2;i++){
            for(Piece localp : this.jeu[i].pieces){
                if(localp.equals(p)){
                    this.jeu[i].pieces.remove(localp);
                    if(s=="Reine"){
                        JOptionPane.showMessageDialog(null,
                                "Pion "+(p.getCouleur()? "Noir" : "Blanc")+" promu Reine !",
                                "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                        this.jeu[i].pieces.add(new Reine(p.getCouleur(), p.x, p.y));
                        // On break la boucle pour ne pas passer � l'�l�ment suivant celui qui vient d'�tre supprim�
                        break;
                    }else{
                        if(s=="Cavalier"){
                            JOptionPane.showMessageDialog(null,
                                    "Pion "+(p.getCouleur()? "Noir" : "Blanc")+" promu Cavalier !",
                                    "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                            this.jeu[i].pieces.add(new Cavalier(p.getCouleur(), p.x, p.y));
                            // On break la boucle pour ne pas passer � l'�l�ment suivant celui qui vient d'�tre supprim�
                            break;
                        }else{
                            if(s=="Fou"){
                                JOptionPane.showMessageDialog(null,
                                        "Pion "+(p.getCouleur()? "Noir" : "Blanc")+" promu Fou !",
                                        "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                this.jeu[i].pieces.add(new Fou(p.getCouleur(), p.x, p.y));
                                // On break la boucle pour ne pas passer � l'�l�ment suivant celui qui vient d'�tre supprim�
                                break;
                            }else{
                                if(s=="Tour"){
                                    JOptionPane.showMessageDialog(null,
                                            "Pion "+(p.getCouleur()? "Noir" : "Blanc")+" promu Tour !",
                                            "Bon coup", JOptionPane.INFORMATION_MESSAGE);
                                    this.jeu[i].pieces.add(new Tour(p.getCouleur(), p.x, p.y));
                                    // On break la boucle pour ne pas passer � l'�l�ment suivant celui qui vient d'�tre supprim�
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return p;
    }
    
    public void executer(Coup coup)
	{
		for(int i = 0 ; i < this.jeu[0].getPieces().size() ; i++)
		{
			if(this.jeu[0].getPieces().get(i).getX() == coup.xInit && this.jeu[0].getPieces().get(i).getY() == coup.yInit)
			{
				this.deplacer(this.jeu[0].getPieces().get(i), coup.xFin, coup.yFin, false);
			}
		}
		for(int i = 0 ; i < this.jeu[1].getPieces().size() ; i++)
		{
			if(this.jeu[1].getPieces().get(i).getX() == coup.xInit && this.jeu[1].getPieces().get(i).getY() == coup.yInit)
			{
				this.deplacer(this.jeu[1].getPieces().get(i), coup.xFin, coup.yFin, false);
			}
		}
	}
    
	public ArrayBlockingQueue<Coup> getFile()
	{
		return file;
	}

	public void setFile(ArrayBlockingQueue<Coup> file)
	{
		this.file = file;
	}
	
    public boolean SeMetEnEchec(Piece p, int x, int y){
        boolean clr=p.getCouleur(), SeMetEnEchec=false;
        int jeu,i,j, memX, memY;
        Piece tmp;
        
        if(clr) // Cas du d�placement d'une pi�ce noire
            jeu=0; // On va parcourir les pi�ces blanches
        else
            jeu=1; // On va parcourir les pi�ces noires
        // On sauvegarde la position initiale
        memX = p.x;
        memY = p.y;
        // On simule le d�placement
        p.x=x;
        p.y=y;
        for(Piece localp : this.jeu[jeu].pieces){
            //Parcours des colonnes
            for(i=0; i<8; i++){
                // Parcours des lignes
                for(j=0; j<8; j++){
                    // Si le d�placement est potentielement correct
                    if(this.deplacer(localp, i, j, true)){
                        // On r�cup�re une �ventuelle pi�ce pr�sente
                        tmp=this.estOccupee(i, j);
                        // Si le d�placement entraine l'acc�s au roi
                        if(tmp != null && tmp instanceof Roi){
                            SeMetEnEchec=true;
                        }
                    }
                }
            }
        }
        // On annule le d�placement
        p.x=memX;
        p.y=memY;
        if(SeMetEnEchec){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean MetEnEchec(boolean clr, int x, int y){
        boolean MetEnEchec=false;
        int jeu,i,j;
        Piece tmp;
        
        if(clr) // Cas du d�placement d'une pi�ce noire
            jeu=1; // On va parcourir les pi�ces noires
        else
            jeu=0; // On va parcourir les pi�ces blanches
        for(Piece localp : this.jeu[jeu].pieces){
            //Parcours des colonnes
            for(i=0; i<8; i++){
                // Parcours des lignes
                for(j=0; j<8; j++){
                    // Si le d�placement est potentielement correct
                    if(this.deplacer(localp, i, j, true)){
                        // On r�cup�re une �ventuelle pi�ce pr�sente
                        tmp=this.estOccupee(i, j);
                        // Si le d�placement entraine l'acc�s au roi
                        if(tmp != null && tmp instanceof Roi){
                            MetEnEchec=true;
                        }
                    }
                }
            }
        }
        if(MetEnEchec){
            return true;
        }else{
            return false;
        }
    }
}