package echec.jeu;

@SuppressWarnings("serial")
public class Fou extends Piece{
	
	public Fou(boolean jeu, int x, int y)
	{
		super(jeu, x, y);
	}
	
	public boolean deplacer(int x, int y){
		int tempx=x-this.x, tempy=y-this.y;
		if( ((x>=0 || x<8) && (x>=0 || x<8)) &&
		(Math.abs(tempx)-Math.abs(tempy)==0 && tempx!=0)){
			//System.out.println("deplacer fou return true");
			return true;
		}
		//System.out.println("deplacer fou return false");
		return false;
	}
}