package echec.jeu;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public abstract class Piece extends JLabel implements ComportementPiece {

    public int x;
    public int y;
    public boolean couleur;
    
    public Piece(boolean equipe, int x, int y)
    {
        super();
        this.couleur = equipe;
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public boolean getCouleur()
    {
        return this.couleur;
    }
    @Override
    public abstract boolean deplacer(int x, int y);
}
