package echec.jeu;

public interface ComportementPiece {

	public boolean deplacer(int x, int y);

}