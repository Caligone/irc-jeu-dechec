package echec.jeu;

@SuppressWarnings("serial")
public class Roi extends Piece{
	
	public boolean moved;
	
	public Roi(boolean jeu, int x, int y)
	{
		super(jeu, x, y);
		moved=false;
	}
	
	
	public boolean deplacer(int x, int y){
		if(((x>=0 || x<8) && (x>=0 || x<8)) && (
		(y==this.y && x==this.x-1) ||
		(y==this.y && x==this.x+1) ||
		(y==this.y-1 && x==this.x) ||
		(y==this.y+1 && x==this.x) ||
		(y==this.y+1 && x==this.x+1) ||
		(y==this.y+1 && x==this.x-1) ||
		(y==this.y-1 && x==this.x+1) ||
		(y==this.y-1 && x==this.x-1) ||
		(y==this.y && x==this.x+2) || // cas roque droit
		(y==this.y && x==this.x-3) )){ //cas roque gauche
			//System.out.println("deplacer roi return true");
			return true;
		}
		//System.out.println("deplacer roi return false");
		return false;
	}
}