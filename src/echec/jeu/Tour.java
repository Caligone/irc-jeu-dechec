package echec.jeu;

@SuppressWarnings("serial")
public class Tour extends Piece{
	
	public boolean moved;
	
	public Tour(boolean jeu, int x, int y)
	{
		super(jeu, x, y);
	}
	
	
	public boolean deplacer(int x, int y){
		if( ((x>=0 || x<8) && (x>=0 || x<8)) &&
		((y==this.y) ||
		(x==this.x) )){
			//System.out.println("deplacer tour return true");
			return true;
		}
		//System.out.println("deplacer tour return false");
		return false;
	}
}