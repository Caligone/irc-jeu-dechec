package echec.jeu;

import java.io.Serializable;

/**
 * Represente un deplacement en memoire
 */
@SuppressWarnings("serial")
public class Deplacement implements Serializable
{
	Piece piece;
	int xInit;
	int yInit;
	int xFin;
	int yFin;
	
	/**
	 * Constructeur de deplacement
	 * @param piece
	 * @param xInit
	 * @param yInit
	 * @param xFin
	 * @param yFin
	 */
	public Deplacement(Piece piece, int xInit, int yInit, int xFin, int yFin)
	{
		this.piece = piece;
		this.xInit = xInit;
		this.yInit = yInit;
		this.xFin = xFin;
		this.yFin = yFin;
	}

	/**
	 * Retourne la piece concernee
	 * @return piece
	 */
	public Piece getPiece() {
		return piece;
	}
	
	/**
	 * Defini la piece concernee
	 * @param piece
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	
	/**
	 * Retourne la coordonnees x initiale
	 * @return xInit
	 */
	public int getxInit() {
		return xInit;
	}
	
	/**
	 * Defini la coordonnees x initiale
	 * @param xInit
	 */
	public void setxInit(int xInit) {
		this.xInit = xInit;
	}
	
	/**
	 * Retourne la coordonnees y initiale
	 * @return yInit
	 */
	public int getyInit() {
		return yInit;
	}
	
	/**
	 * Defini la coordonnees y initiale
	 * @param yInit
	 */
	public void setyInit(int yInit) {
		this.yInit = yInit;
	}
	
	/**
	 * Retourne la coordonnees x finale
	 * @return xFin
	 */
	public int getxFin() {
		return xFin;
	}
	
	/**
	 * Defini la coordonnees x finale
	 * @return xFin
	 */
	public void setxFin(int xFin) {
		this.xFin = xFin;
	}
	
	/**
	 * Retourne la coordonnees y finale
	 * @return yFin
	 */
	public int getyFin() {
		return yFin;
	}
	/**
	 * Defini la coordonnees y finale
	 * @return yFin
	 */
	public void setyFin(int yFin) {
		this.yFin = yFin;
	}
	
}
