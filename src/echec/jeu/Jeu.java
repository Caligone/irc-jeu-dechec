package echec.jeu;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Jeu implements Serializable {

	public Boolean equipe;
	public ArrayList<Piece> pieces;

	public Jeu(boolean equipe)
	{
		this.equipe = equipe;
		this.pieces = new ArrayList<Piece>();
		
		if(this.equipe)
		{
			pieces.add(new Tour(this.equipe, 0, 0));
			pieces.add(new Tour(this.equipe, 7, 0));
			pieces.add(new Cavalier(this.equipe, 1, 0));
			pieces.add(new Cavalier(this.equipe, 6, 0));
			pieces.add(new Fou(this.equipe, 2, 0));
			pieces.add(new Fou(this.equipe, 5, 0));
			pieces.add(new Reine(this.equipe, 3, 0));
			pieces.add(new Roi(this.equipe, 4, 0));
			for(int i = 0 ; i < 8 ; i++)
				pieces.add(new Pion(this.equipe, i, 1));
		}
		else
		{
			pieces.add(new Tour(this.equipe, 0, 7));
			pieces.add(new Tour(this.equipe, 7, 7));
			pieces.add(new Cavalier(this.equipe, 1, 7));
			pieces.add(new Cavalier(this.equipe, 6, 7));
			pieces.add(new Fou(this.equipe, 2, 7));
			pieces.add(new Fou(this.equipe, 5, 7));
			pieces.add(new Reine(this.equipe, 3, 7));
			pieces.add(new Roi(this.equipe, 4, 7));
			for(int i = 0 ; i < 8 ; i++)
				pieces.add(new Pion(this.equipe, i, 6));
		}
	}
	
	public ArrayList<Piece> getPieces()
	{
		return this.pieces;
	}

	public boolean getEquipe() {
		return this.equipe;
	}
}