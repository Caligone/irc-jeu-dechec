package echec.jeu;

import java.util.Scanner;

import echec.ihm.Fenetre;

public class Partie {

	Echiquier e;

	public static void main(String[] args) {
		Partie p = new Partie();
		p.playGraphic();
		//p.playConsole();
	}

	public Partie()
	{
		e = new Echiquier();
	}
	
	public void playGraphic()
	{
		new Fenetre();
	}

	@SuppressWarnings("unused")
	public void playConsole() {
		int i = 0,initx,inity,destx,desty;
		String SCouleur="Blanc",err;
		Piece p;
		boolean b=false,BCouleur=true;
		Scanner sc = new Scanner(System.in);
		while (i != 10) {
			displayTray();
			do{
				do{
					try{
						System.out.println("Joueur \""+SCouleur+"\", c'est a vous !");
						System.out.print("Entrer la colonne de la piece a deplacer : ");
						initx = sc.nextInt();
						System.out.print("Entrer la ligne de la piece a deplacer : ");
						inity = sc.nextInt();
						p=e.estOccupee(initx, inity);
						if(p==null || p.getCouleur()==BCouleur)
							System.out.println("Il n'y a pas de piece de votre couleur e cet endroit... Recommencez");
					}catch(Exception e){
						System.out.println("ERREUR : La valeur saisie n'est pas valide. Recommencez...");
						err=sc.nextLine();
						p=null;
					}
				}while(p==null || p.getCouleur()==BCouleur);
				try{
					System.out.print("Entrer la colonne de destination : ");
					destx = sc.nextInt();
					System.out.print("Entrer la ligne de destination : ");
					desty = sc.nextInt();
					b=e.deplacer(p, destx, desty, false);
					if(b==false)
						System.out.println("Deplacement impossible. Recommencez...");
				}catch(Exception e){
					System.out.println("ERREUR : La valeur saisie n'est pas valide. Recommencez...");
					err=sc.nextLine();
					p=null;
				}
			}while(b==false);
			if(BCouleur){
				BCouleur=false;
				SCouleur="Noir";
			}else{
				BCouleur=true;
				SCouleur="Blanc";
			}
		}
	}

	public void displayTray() {
		int i, j;
		Piece tmp;
		System.out.println("   |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |");
		System.out.println("---|-----|-----|-----|-----|-----|-----|-----|-----|");
		for (i = 0; i < 8; i++) {
			System.out.print(i + "  |  ");
			for (j = 0; j < 8; j++) {
				tmp = e.estOccupee(j, i);
				if (tmp instanceof Pion)
					if (tmp.getCouleur())
						System.out.print("♟");
					else
						System.out.print("♙");
				else if (tmp instanceof Roi)
					if (tmp.getCouleur())
						System.out.print("♚");
					else
						System.out.print("♔");
				else if (tmp instanceof Reine)
					if (tmp.getCouleur())
						System.out.print("♛");
					else
						System.out.print("♕");
				else if (tmp instanceof Tour)
					if (tmp.getCouleur())
						System.out.print("♜");
					else
						System.out.print("♖");
				else if (tmp instanceof Fou)
					if (tmp.getCouleur())
						System.out.print("♝");
					else
						System.out.print("♗");
				else if (tmp instanceof Cavalier)
					if (tmp.getCouleur())
						System.out.print("♞");
					else
						System.out.print("♘");
				else
					System.out.print(" ");
				System.out.print("  |  ");
			}
			System.out.print("\n");
			System.out.println("---|-----|-----|-----|-----|-----|-----|-----|-----|");
		}
	}
}
