package echec.utilitaires;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import echec.jeu.*;

/**
 * Classe utilitaire offrant un ensemble de fonctions utiles pour le rendu sur l'interface graphique
 */
public abstract class UtilitaireRendu
{
	/**
	 * Methode de conversion de l'index du composant en coordonnee X
	 * @param index du composant
	 * @return Coordonnee X sur l'echiquier
	 */
	public static int getXFromIndex(int index)
	{
		if(index > 63 || index < 0)
			return -1;
		return index%8;
	}
	
	/**
	 * Methode de conversion de l'index du composant en coordonnee Y
	 * @param index du composant
	 * @return Coordonnee Y sur l'echiquier
	 */
	public static int getYFromIndex(int index)
	{
		if(index > 63 || index < 0)
			return -1;
		return Math.abs(index/8);
	}
	
	/**
	 * Methode de conversion des coordonnees de l'echiquier en index de composant
	 * @param x coordonnee X, y coordonnee Y
	 * @return index de composant
	 */
	public static int getIndexFromXY(int x, int y)
	{
		return y*8+x;
	}
	
	/**
	 * Methode permettant de trouver l'icone correspondant e une piece
	 * @param piece
	 * @return L'icone correspondante e la piece
	 */
	public static Icon getRendu(Piece piece)
	{
		Icon rendu;
		
		if(piece instanceof Cavalier)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/cavalierNoirS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/cavalierBlancS.png"));
		}
		else if(piece instanceof Fou)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/fouNoirS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/fouBlancS.png"));
		}
		else if(piece instanceof Pion)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/pionNoirS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/pionBlancS.png"));
		}
		else if(piece instanceof Reine)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/reineNoireS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/reineBlancS.png"));
		}
		else if(piece instanceof Roi)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/roiNoirS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/roiBlancS.png"));
		}
		else if(piece instanceof Tour)
		{
			if(piece.getCouleur())
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/tourNoireS.png"));
			else
				rendu = new ImageIcon(piece.getClass().getResource("/ressources/tourBlancS.png"));
		}
		else
			return null;
		return rendu;
	}
	
	/**
	 * Methode permettant de convertir la position de la souris en index de composant
	 * @param x Coordonnee X de la souris
	 * @param y Coordonnee Y de la souris
	 * @return l'index correspondant aux coordonnees
	 */
	public static int getIndexFromMousePosition(int x, int y)
	{
		int xPiece = x/75;
		int yPiece = y/75;
		return UtilitaireRendu.getIndexFromXY(xPiece, yPiece);
	}
}
