package echec.ihm;

import java.awt.Color;
import java.io.Serializable;

import javax.swing.JLabel;

/**
 * Repr�sente une case d'un point de vue graphique
 */
@SuppressWarnings("serial")
public class Case extends JLabel implements Serializable {

	int x, y;
	
	/**
	 * 
	 * @param background Couleur du fond de la case
	 * @param x Coordonnee x de la case
	 * @param y Coordonnee y de la case
	 */
	public Case(Color background, int x, int y) {
		super();
		this.setBackground(background);
		this.x = x;
		this.y = y;
	}
	
}
