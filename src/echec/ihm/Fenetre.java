package echec.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import echec.jeu.Echiquier;
import echec.jeu.Jeu;
import echec.jeu.Piece;
import echec.threads.Connexion;
import echec.utilitaires.UtilitaireRendu;

/**
 * Fenetre de base du jeu
 * Contient un GridLayout pour le plateau de jeu, une JList pour les coups jou�s, un JMenu et une StatusBar
 */
@SuppressWarnings("serial")
public class Fenetre extends JFrame {

	public Echiquier echiquier;
	public JPanel plateau;
	public StatusBar statusBar;
	public JList list;
	public boolean connecte;
	
	public Fenetre()
	{
		super("ChessMe");
		this.connecte = false;
		this.setSize(900, 660);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	
		// Barre de menu
		JMenuBar menuBar = new JMenuBar();
		JMenu partieMenu = new JMenu("Partie");
		JMenuItem nouvellePartie = new JMenuItem("Nouvelle");
		nouvellePartie.setEnabled(false);
		nouvellePartie.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		    	echiquier = new Echiquier();
		    	chargerEchiquier(echiquier);
		    }
		});
		partieMenu.add(nouvellePartie);
		JMenuItem chargerPartie = new JMenuItem("Charger");
		chargerPartie.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		    	JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				  File fileX = fileChooser.getSelectedFile();
				  try{
						FileInputStream fileIS = new FileInputStream(fileX.getAbsolutePath());
						ObjectInputStream in = new ObjectInputStream(fileIS);
						echiquier = (Echiquier)in.readObject();
						chargerEchiquier(echiquier);
						in.close();
					}
					catch(Exception e)
					{}
				}
		    }
		});
		partieMenu.add(chargerPartie);
		JMenuItem sauvegarderPartie = new JMenuItem("Sauvegarde");
		sauvegarderPartie.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		    	JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				  File fileX = fileChooser.getSelectedFile();
				  try {
						FileOutputStream file = new FileOutputStream(fileX.getAbsolutePath());
						ObjectOutputStream out = new ObjectOutputStream(file);
						out.writeObject(echiquier);
						out.close();
						file.close();
					} catch (Exception i) {
						i.printStackTrace();
					}
				}
		    }
		});
		partieMenu.add(sauvegarderPartie);
		final Fenetre fenetre = this;
		JMenuItem connecterPartie = new JMenuItem("Connecter");
		connecterPartie.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		        System.out.println("=== Test Socket ===");
		        String adresseIP = (String)JOptionPane.showInputDialog(
	                    null,
	                    "Saisissez l'adresse IP :\n");
	              //String adresseIP = "127.0.0.1";
		        System.out.println("Tentative de connexion � l'adresse : "+adresseIP+"...");
		        connecte = true;
		        Thread connexionThread = new Thread(new Connexion(adresseIP, echiquier.getFile(), fenetre));
		        connexionThread.start();
		    }
		});
		partieMenu.add(connecterPartie);
		JMenuItem fermerPartie = new JMenuItem("Fermer");
		fermerPartie.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		        System.exit(0);
		    }
		});
		partieMenu.add(fermerPartie);
		menuBar.add(partieMenu);
		
		JMenu coupsMenu = new JMenu("Coups");
		JMenuItem annuler = new JMenuItem("Annuler");
		annuler.setEnabled(false);
		annuler.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		        System.out.println("Annuler");
		    }
		});
		coupsMenu.add(annuler);
		JMenuItem refaire = new JMenuItem("Refaire");
		refaire.setEnabled(false);
		refaire.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent evt) {
		        System.out.println("Refaire");
		    }
		});
		coupsMenu.add(refaire);
		menuBar.add(coupsMenu);
		this.setJMenuBar(menuBar);
		this.setLayout(new BorderLayout());

		this.echiquier = new Echiquier();
		
		// Historique de coups
		list = new JList(this.echiquier.getListeCoup());
		list.setSize(300, 600);
		list.setLocation(600, 0);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list.setVisibleRowCount(-1);

		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(300, 600));
		this.getContentPane().add(listScroller, BorderLayout.LINE_END);
		
		// Plateau de jeu
		this.plateau = new JPanel();
		plateau.setLayout(new GridLayout(8,8));
		plateau.setPreferredSize(new Dimension(600, 600));
		plateau.setBackground(Color.BLACK);
		
		ChessMouseListener listener = new ChessMouseListener(this.echiquier);
		plateau.addMouseListener(listener);
		
		for(int y = 0 ; y < 8 ; y++)
			for(int x = 0 ; x < 8 ; x++)
			{
				Case caseEnCours;
				if((x+y)%2 != 0)
					caseEnCours = new Case(Color.WHITE, x, y);
				else
					caseEnCours = new Case(Color.BLACK, x, y);
				caseEnCours.setHorizontalAlignment(JLabel.CENTER);
				caseEnCours.setVerticalAlignment(JLabel.CENTER);
				caseEnCours.setOpaque(true);
				plateau.add(caseEnCours);
			}
		
		chargerEchiquier(this.echiquier);
		
		this.getContentPane().add(plateau, BorderLayout.LINE_START);
		this.statusBar = new StatusBar();
		this.getContentPane().add(statusBar, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	/**
	 * M�thode de chargement d'un �chiquier sur le plateau de jeu
	 * @param echiquier
	 */
	public void chargerEchiquier(Echiquier echiquier)
	{
		this.echiquier = echiquier;
		list.repaint();
		
		Jeu jeuNoir;
		Jeu jeuBlanc;
		
		// Suppression de toutes les icones
		for(int i = 0 ; i < 64 ; i++)
			((JLabel)this.plateau.getComponent(i)).setIcon(null);
		
		// Parcours des deux jeux et placement des pieces
		if(this.echiquier.getJeu()[0].getEquipe())
		{
			jeuBlanc = this.echiquier.getJeu()[1];
			jeuNoir = this.echiquier.getJeu()[0];
		}
		else
		{
			jeuBlanc = this.echiquier.getJeu()[0];
			jeuNoir = this.echiquier.getJeu()[1];
		}
		
		for(Piece piece : jeuBlanc.getPieces())
		{
			if(piece.getX() >= 0 && piece.getY() >= 0)
			{
				JLabel tmp = (JLabel)plateau.getComponent(UtilitaireRendu.getIndexFromXY(piece.getX(), piece.getY()));
				tmp.setIcon(UtilitaireRendu.getRendu(piece));
				tmp.repaint();
			}
		}
		
		for(Piece piece : jeuNoir.getPieces())
		{
			if(piece.getX() >= 0 && piece.getY() >= 0)
			{
				JLabel tmp = (JLabel)plateau.getComponent(UtilitaireRendu.getIndexFromXY(piece.getX(), piece.getY()));
				tmp.setIcon(UtilitaireRendu.getRendu(piece));
				tmp.repaint();
			}
		}
	}
}
