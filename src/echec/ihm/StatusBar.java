package echec.ihm;

import java.awt.Dimension;

import javax.swing.JLabel;


@SuppressWarnings("serial")
public class StatusBar extends JLabel {
    
    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 16));
        setMessage("Les blancs commencent");
    }
    
    public void setMessage(String message) {
        setText(" "+message);        
    }        
}