package echec.ihm;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import echec.jeu.Coup;
import echec.jeu.Echiquier;
import echec.jeu.Piece;
import echec.utilitaires.UtilitaireRendu;

public class ChessMouseListener extends MouseAdapter
{
	Echiquier echiquier;
	Piece pieceEnCours;
	Case caseInitiale;

	public ChessMouseListener(Echiquier echiquier)
	{
		super();
		this.echiquier = echiquier;
		this.caseInitiale = null;
	}

	public void mouseClicked(MouseEvent e)
	{
		// Selection d'une piece
		if (this.caseInitiale == null)
		{
			Component com = ((JPanel) e.getSource()).getComponent(UtilitaireRendu.getIndexFromMousePosition(e.getPoint().x, e.getPoint().y));
			if (!(com instanceof Case))
				return;
			this.pieceEnCours = echiquier.getPieceFromXY(((Case) com).x, ((Case) com).y);
			if (this.pieceEnCours != null)
				this.caseInitiale = (Case) com;
			else
				return;
			if(this.pieceEnCours.getCouleur() != this.echiquier.tour)
			{
				this.pieceEnCours = null;
				this.caseInitiale = null;
				return;
			}
			if (e.getComponent().getParent().getParent().getParent().getParent() instanceof Fenetre)
			{
				Fenetre fenetre = (Fenetre) e.getComponent().getParent().getParent().getParent().getParent();
				fenetre.statusBar.setMessage(this.pieceEnCours.getClass().getSimpleName() + " " + (this.pieceEnCours.getCouleur() ? "Noir" : "Blanc") + "(" + this.pieceEnCours.x + ", " + this.pieceEnCours.y + ") selectionne");
			}
		}
		// Deplacement d'une piece
		else
		{
			if (this.pieceEnCours.getCouleur() != this.echiquier.tour)
				return;

			Component com = ((JPanel) e.getSource()).getComponent(UtilitaireRendu.getIndexFromMousePosition(e.getPoint().x, e.getPoint().y));
			if (!(com instanceof Case))
				return;
			if (this.echiquier.deplacer(this.pieceEnCours, ((Case) com).x, ((Case) com).y, false))
			{
				// Rechargement du plateau
				if (e.getComponent().getParent().getParent().getParent().getParent() instanceof Fenetre)
				{
					Fenetre fenetre = (Fenetre) e.getComponent().getParent().getParent().getParent().getParent();
					fenetre.chargerEchiquier(this.echiquier);
				}

				// Stockage d'un coup
				this.echiquier.getListeCoup().add(0, this.pieceEnCours.getClass().getSimpleName() + " " + (this.pieceEnCours.getCouleur() ? "Noir" : "Blanc") + " (" + this.caseInitiale.x + ", " + this.caseInitiale.y + ") va en (" + ((Case) com).x + ", " + ((Case) com).y + ")");

				if (e.getComponent().getParent().getParent().getParent().getParent() instanceof Fenetre)
				{
					Fenetre fenetre = (Fenetre) e.getComponent().getParent().getParent().getParent().getParent();
					if (!fenetre.connecte)
					{
						this.echiquier.tour = !this.echiquier.tour;
					}
					else
					{
						try
						{
							this.echiquier.getFile().put(new Coup(this.pieceEnCours, this.caseInitiale.x, this.caseInitiale.y, ((Case) com).x, ((Case) com).y));
						} catch (Exception e1)
						{
							e1.getStackTrace();
						}
					}
					fenetre.statusBar.setMessage("Aux " + (this.pieceEnCours.getCouleur() ? "Blancs" : "Noirs") + " de jouer");
				}
			} else
			{
				JOptionPane.showMessageDialog(null, "Ce deplacement est impossible !", "Erreur de deplacement", JOptionPane.WARNING_MESSAGE);
			}
			this.caseInitiale = null;
		}
	}
}
